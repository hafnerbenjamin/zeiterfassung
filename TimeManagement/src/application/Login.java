package application;

import java.util.ArrayList;
import java.util.Optional;

import BLL.BLLPerson;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import model.Person;

public class Login {

	static BLLPerson bllPerson = new BLLPerson();
	static public Person LOGIN_PERSON = new Person();
	static boolean loginSuccsessfull;

	public static Dialog<Pair<String, String>> setLoginWindow(){		
		// Create the custom dialog.
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle("Login Dialog");
		dialog.setHeaderText("Login f�rs Zeitsystem");

		// Set the button types.
		ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
		ButtonType abortButtonType = new ButtonType("Abbrechen", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().setAll(loginButtonType, abortButtonType);
			
		// Create the tocken and password labels and fields.
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField tocken = new TextField();
		tocken.setPromptText("K�rzel");
		PasswordField password = new PasswordField();
		password.setPromptText("Pin");

		grid.add(new Label("K�rzel:"), 0, 0);
		grid.add(tocken, 1, 0);
		grid.add(new Label("Pin:"), 0, 1);
		grid.add(password, 1, 1);

		// Enable/Disable login button depending on whether a tocken was entered.
		Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);

		// Do some validation (using the Java 8 lambda syntax).
		tocken.textProperty().addListener((observable, oldValue, newValue) -> {
		    loginButton.setDisable(newValue.trim().isEmpty());
		});

		dialog.getDialogPane().setContent(grid);

		// Request focus on the tocken field by default.
		Platform.runLater(() -> tocken.requestFocus());

		// Convert the result to a tocken-password-pair when the login button is clicked.
		dialog.setResultConverter(dialogButton -> {
		    if (dialogButton == loginButtonType) {
		        return new Pair<>(tocken.getText(), password.getText());
		    }
		    return null;
		});

		return dialog;
	}
	
	
	public static boolean loginSucsses(Optional<Pair<String, String>> result){
		loginSuccsessfull = false;
		result.ifPresent(tockenPassword -> {
			if(validateLogin(tockenPassword.getKey(), tockenPassword.getValue())){
				loginSuccsessfull = true;
			}
		});
		
	    return loginSuccsessfull;
	}
	
	
	private static boolean validateLogin(String tocken, String password){
		ArrayList<Person> persons = new ArrayList<>();
		persons.addAll(bllPerson.get());
        
		for(Person p : persons){
			if(p.getToken().equals(tocken)){
				System.out.println("Person found: " + p.getFirstName() +" "+ p.getLastName());
				if(p.getPin().equals(password)){
					LOGIN_PERSON = p;
					return true;
				}else{
					System.out.println("Password wrong");
					break;
				}
			}
		}
		return false;
	}
}
