package application;
	
import java.io.IOException;
import java.util.Optional;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.util.Pair;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;


public class Main extends Application {
	
	private AnchorPane rootLayout;
    private Stage primaryStage;
	private Login login = new Login();
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        openApp();
	}
	
	private void openApp(){
		try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/mainScreen.fxml"));
            rootLayout = (AnchorPane) loader.load();
                        
			connectScenes("/timeMgmt.fxml", 0);
			connectScenes("/reporting.fxml", 1);
			connectScenes("/personMgmt.fxml", 2);
			connectScenes("/teamMgmt.fxml", 3);
			connectScenes("/projectMgmt.fxml", 4);
			connectScenes("/payMgmt.fxml", 5);
			
			Scene scene = new Scene(rootLayout);
			primaryStage.setTitle("Zeiterfassung");
			primaryStage.getIcons().add(new Image("/logo.png"));
			primaryStage.setScene(scene);
			primaryStage.show();
			
			boolean loginOk = false;
			
			while(!loginOk){
				Dialog<Pair<String, String>> dialog = Login.setLoginWindow();
				Optional<Pair<String, String>> result = dialog.showAndWait();

				if (login.loginSucsses(result)){
					primaryStage.setTitle("Zeiterfassung von: " + Login.LOGIN_PERSON.getFirstName() +" "+ Login.LOGIN_PERSON.getLastName());
					loginOk = true;
				}else{
			        System.exit(0);
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	

    private void connectScenes(String loc, int menuePos) throws IOException {
        TabPane menu = (TabPane) rootLayout.getChildren().get(0);
        AnchorPane anchorPane;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource(loc));
        anchorPane = loader.load();
        menu.getTabs().get(menuePos).setContent(anchorPane);
    }
}
