package DAL;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import BLL.BLLPerson;
import BLL.BLLProject;
import BLL.BLLServiceType;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Team;
import model.TimeEntry;
import utility.DBUtility;

public class DBTimeEntry {
	
	private TimeEntry timeEntry;
	private static BLLProject projects = new BLLProject();
	private static BLLPerson persons = new BLLPerson();
	private static BLLServiceType serviceTypes = new BLLServiceType();
	
	public static ObservableList<TimeEntry> get(Long id) {
		String stat = "SELECT timeEntryId, WorkedHours, Date, ProjectId, PersonId, Comment, Active, ServiceTypeId FROM TimeEntry ";
		if (id != null){
			stat += "where timeEntryId = ?";
		}
        ObservableList<TimeEntry> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            if (id != null) { statement.setLong(1, id); }
            ResultSet set = statement.executeQuery();
            mapTimeEntry(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
	}

	
	
    private static void mapTimeEntry(ObservableList<TimeEntry> result, ResultSet set) throws SQLException
    {
        while (set.next())
        {
        	TimeEntry timeEntry = new TimeEntry(
                    set.getLong(1),
                    set.getInt(2),
                    set.getDate(3).toLocalDate(),
                    projects.getById(set.getLong(4)),
                    persons.getById((set.getLong(5))),
                    set.getString(6),
                    set.getBoolean(7),
                    serviceTypes.getById((set.getLong(8)))
            );
            result.add(timeEntry);
        }
    }

    public static Long getProjectDetail(TimeEntry timeEntry)
    {		
    	String stat = "select projectDetailId from projectdetail as pd " +
					  "join project as p on p.projectId = pd.ProjectId " +
					  "join person as pers on pers.PersonId = pd.PersonId " +
					  "join Role as r on r.RoleId = pd.RoleId " +
					  "where p.projectId = ? and pers.PersonId = ? and r.RoleId = ?; ";
	    try (Connection conn = DBUtility.createConnection())
	    {
	        PreparedStatement statement = conn.prepareStatement(stat);
            statement.setLong(1, timeEntry.getProject().getProjectId());
            statement.setLong(2, timeEntry.getPerson().getPersonId());
            statement.setLong(3, timeEntry.getRole().getRoleId());
	        ResultSet set = statement.executeQuery();
	        if(set.next())
	        	return set.getLong(1);
	    } catch(SQLException e) {
	        e.printStackTrace();
	        return null;
	    }
	    return null;
    }
    
	public static Long add(TimeEntry timeEntry, Long projectDetailId) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "insert into TimeEntry (WorkedHours, Date, ProjectDetailId, Comment, Active) values (?, ?, ?, ?, ?);",
                    Statement.RETURN_GENERATED_KEYS);
           statement.setInt(1, timeEntry.getHours());
           Date date = Date.valueOf(timeEntry.getDate());
           statement.setDate(2, date);
           statement.setLong(3, projectDetailId);
           statement.setString(4, timeEntry.getComment());
           statement.setBoolean(5, timeEntry.getActive());
           statement.executeUpdate();
           ResultSet rs = statement.getGeneratedKeys();
           if (rs.next()) {
               return rs.getLong(1);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return (long) 0;	
	}

	public static Long addProjectDetail(TimeEntry timeEntry)
	{
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "insert into ProjectDetail (RoleId, PersonId, ProjectId) values (?, ?, ?);",
                    Statement.RETURN_GENERATED_KEYS);
           statement.setLong(1, timeEntry.getRole().getRoleId());
           statement.setLong(2, timeEntry.getPerson().getPersonId());
           statement.setLong(3, timeEntry.getProject().getProjectId());
           statement.executeUpdate();
           ResultSet rs = statement.getGeneratedKeys();
           if (rs.next()) {
               return rs.getLong(1);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return (long) 0;	
	}
	
	public static void update(TimeEntry timeEntry, Long projectDetailId) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "update timeEntry set " +
                   		 "WorkedHours = ?, " +
                         "Date = ?, " +
                         "Comment = ?, " +
                         "ProjectDetailId = ?, " +
                         "Active = ? " +
                        "where teamEntryId = ?;",
                    Statement.NO_GENERATED_KEYS);
			statement.setInt(1, timeEntry.getHours());
	        Date date = Date.valueOf(timeEntry.getDate());
            statement.setDate(2, date);
            statement.setString(3, timeEntry.getComment());
            statement.setLong(4, projectDetailId);
            statement.setBoolean(5, timeEntry.getActive());
            statement.setLong(6, timeEntry.getTimeEntryId());
            statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }	
	}

	public static void delete(TimeEntry timeEntry) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "delete FROM timeEntry " +
                            "where timeEntryId = ?;",
                    Statement.SUCCESS_NO_INFO);
           statement.setLong(1, timeEntry.getTimeEntryId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
	}
}
