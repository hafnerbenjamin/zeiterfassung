package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Person;
import model.ServiceType;
import model.Team;
import utility.DBUtility;
import utility.EmployeeType;

public class DBTeam {

	public static ObservableList<Team> get(Long id) {
		String stat = "SELECT teamId, teamName, token FROM Team ";
		if (id != null){
			stat += "where teamId = ?";
		}
        ObservableList<Team> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            if (id != null) { statement.setLong(1, id); }
            ResultSet set = statement.executeQuery();
            mapTeam(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
	}

    private static void mapTeam(ObservableList<Team> result, ResultSet set) throws SQLException
    {
        while (set.next())
        {
        	Team team = new Team(
                    set.getLong(1),
                    set.getString(2),
                    set.getString(3),
                    getPersonsForTeam(set.getLong(1))
            );
            result.add(team);
        }
    }

    private static ObservableList<Person> getPersonsForTeam(Long teamId){
		String stat = "SELECT personId, teamId, Type FROM personTeam ";
			stat += "where teamId = ?";
			ObservableList<Person> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            statement.setLong(1, teamId);
            ResultSet set = statement.executeQuery();
            
            while(set.next())
            {
            	Person person = DBPerson.get(set.getLong(1)).get(0);
            	if(set.getInt(3) == 1){            		
            		person.setTeamFunction(EmployeeType.TeamLeader);
            	}else{
            		person.setTeamFunction(EmployeeType.Employee);
            	}
            	result.add(person);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public static long getTeamForPerson(Long personId){
		String stat = "SELECT personId, teamId, Type FROM personTeam ";
			stat += "where personId = ?";
			long teamId = 0;
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            statement.setLong(1, personId);
            ResultSet set = statement.executeQuery();
            while(set.next()){
            	teamId = set.getLong(2);
            }
        	
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return teamId;
    }
    
	public static Long add(Team team) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "insert into team (teamName, token) values (?, ?);",
                    Statement.RETURN_GENERATED_KEYS);
           statement.setString(1, team.getName());
           statement.setString(2, team.getToken());
           statement.executeUpdate();
           addOrUpdatePersonToTeam(team);
           ResultSet rs = statement.getGeneratedKeys();
           if (rs.next()) {
               return rs.getLong(1);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return (long) 0;		
	}

	public static void update(Team team)
	{
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "update team set " +
                   		 "teamName = ?, " +
                    	 "token = ? " +
                        "where teamId = ?;",
                    Statement.NO_GENERATED_KEYS);
			statement.setString(1, team.getName());
           statement.setString(2, team.getToken());
           statement.setLong(3, team.getTeamId());
           statement.executeUpdate();
           addOrUpdatePersonToTeam(team);
        } catch (SQLException e) {
            e.printStackTrace();
        }	
	}
	
	public static void addOrUpdatePersonToTeam(Team team)
	{
        try (Connection conn = DBUtility.createConnection()) {
        	String stm;
        	ObservableList<Person> persons = team.getPersons();
        	for (Person person : persons) {
				if(getTeamForPerson(person.getPersonId()) == 0)
				{
					stm = "insert into personteam (teamId, type, personId) values (?, ?, ?)";
				}
				else
				{
					stm = "update personteam set teamId = ?, type = ? where personId = ?";
				}
	            PreparedStatement statement = conn.prepareStatement(stm,
	                    Statement.NO_GENERATED_KEYS);
	           statement.setLong(1, team.getTeamId());
	           statement.setInt(2, person.getTeamFunction().ordinal());
	           statement.setLong(3, person.getPersonId());
	           statement.executeUpdate();
			}
       } catch (SQLException e) {
           e.printStackTrace();
       }	
	}
	
	public static void delete(Team team) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "delete FROM team " +
                            "where teamId = ?;",
                    Statement.SUCCESS_NO_INFO);
           statement.setLong(1, team.getTeamId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
	}
}
