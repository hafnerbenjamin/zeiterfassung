package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Address;
import model.Person;
import model.Project;
import utility.DBUtility;

public class DBProject {

	public static ObservableList<Project> get(Long id) {
		String stat = "SELECT projectId, ProjectName, Active FROM Project ";
		if (id != null){
			stat += "where projectId = ?";
		}
        ObservableList<Project> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            if (id != null) {
            	statement.setLong(1, id);
            }
            ResultSet set = statement.executeQuery();
            mapProject(result, set);
            
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
	}
	
    private static void mapProject(ObservableList<Project> result, ResultSet set) throws SQLException
    {
        while (set.next())
        {
            Project project = new Project(
                    set.getLong(1),
                    set.getString(2),
                    set.getBoolean(3)
            );
            result.add(project);
        }
    }
	
	public static Project getProjectForTimeEntryId(Long projectDetailId)
	{
		String stat = "SELECT p.projectId, ProjectName, Active, ServiceTypeId FROM Project as p " + 
					  "join projectDetail as pd on pd.ProjectId = p.ProjectId " + 
					  "where pd.projectDetailId = ?";
        ObservableList<Project> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            statement.setLong(1, projectDetailId);
            ResultSet set = statement.executeQuery();
            mapProject(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
	}
    
	public static Long add(Project project) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                     "insert into Project (ProjectName, Active, ServiceTypeId) values (?, ?, ?);",
                     Statement.RETURN_GENERATED_KEYS); 
            statement.setString(1, project.getName());
            statement.setBoolean(2, project.getActive());
            statement.setLong(3, project.getServiceType().getServiceTypeId());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                return rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (long) 0;
	}

	public static void update(Project project) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "update project set " +
                   		 "ProjectName = ?, " +
                         "Active = ? " +
                        "where projectId = ?;",
                    Statement.NO_GENERATED_KEYS);
			statement.setString(1, project.getName());
           statement.setBoolean(2, project.getActive());
           statement.setLong(3, project.getProjectId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }		
	}

	public static void delete(Project project) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "delete FROM project " +
                            "where projectId = ?;",
                    Statement.SUCCESS_NO_INFO);
           statement.setLong(1, project.getProjectId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
	}
}
