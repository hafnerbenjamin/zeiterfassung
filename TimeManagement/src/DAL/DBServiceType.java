package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Role;
import model.ServiceType;
import utility.DBUtility;

public class DBServiceType {

	public static ObservableList<ServiceType> get(Long id) {
		String stat = "SELECT serviceTypeId, name, cost, active FROM ServiceType ";
		if (id != null){
			stat += "where serviceTypeId = ?";
		}
        ObservableList<ServiceType> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            if (id != null) { statement.setLong(1, id); }
            ResultSet set = statement.executeQuery();
            mapServiceType(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
	}

    private static void mapServiceType(ObservableList<ServiceType> result, ResultSet set) throws SQLException
    {
        while (set.next())
        {
        	ServiceType serviceType = new ServiceType(
                    set.getLong(1),
                    set.getString(2),
                    set.getFloat(3),
                    set.getBoolean(4)
            );
            result.add(serviceType);
        }
    }
    
	public static Long add(ServiceType serviceType) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "insert into serviceType (Name, Cost, Active) values (?, ?, ?);",
                    Statement.RETURN_GENERATED_KEYS);
           statement.setString(1, serviceType.getName());
           statement.setFloat(2, serviceType.getCost());
           statement.setBoolean(3, serviceType.getActive());
           statement.executeUpdate();
           ResultSet rs = statement.getGeneratedKeys();
           if (rs.next()) {
               return rs.getLong(1);
           }
       } catch (SQLException e) {
           e.printStackTrace();
       }
       return (long) 0;
	}


	public static void update(ServiceType serviceType) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "update serviceType set " +
                   		 "Name = ?, " +
                         "Cost = ?, " +
                         "Active = ? " +
                        "where serviceTypeId = ?;",
                    Statement.NO_GENERATED_KEYS);
			statement.setString(1, serviceType.getName());
           statement.setFloat(2, serviceType.getCost());
           statement.setBoolean(3, serviceType.getActive());
           statement.setLong(4, serviceType.getServiceTypeId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }	
	}


	public static void delete(ServiceType serviceType) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "delete FROM serviceType " +
                            "where serviceTypeId = ?;",
                    Statement.SUCCESS_NO_INFO);
           statement.setLong(1, serviceType.getServiceTypeId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
	}
}
