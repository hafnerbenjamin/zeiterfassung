package DAL;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.*;
import utility.DBUtility;

import java.sql.*;

public class DBPerson {

	public static ObservableList<Person> get(Long id) {
		String stat = "SELECT personId, Pin, Gender, LastName, FirstName, Token FROM Person ";
		if (id != null){
			stat += "where personId = ?";
		}
        ObservableList<Person> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            if (id != null) {
            	statement.setLong(1, id);
            }
            ResultSet set = statement.executeQuery();
            mapPerson(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
	}
	
	public static Person getPersonForTimeEntryId(Long projectDetailId)
	{
		String stat = "SELECT p.personId, p.Pin, p.Gender, p.LastName, p.FirstName, p.Token FROM Person as p " + 
					  "join projectDetails as pd on pd.PersonId = p.PersonId " + 
					  "where pd.projectDetailId = ?";
        ObservableList<Person> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            statement.setLong(1, projectDetailId);
            ResultSet set = statement.executeQuery();
            mapPerson(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
	}
	
    private static void mapPerson(ObservableList<Person> result, ResultSet set) throws SQLException
    {
        while (set.next())
        {
            Person person = new Person(
                    set.getLong(1),
                    set.getString(2),
                    set.getString(3),
                    set.getString(4),
                    set.getString(5),
                    set.getString(6),
                    new Address(),
                    DBTeam.getTeamForPerson(set.getLong(1))
                    
            );
            result.add(person);
        }
    }
    
	public static Long add(Person person) {
        try (Connection conn = DBUtility.createConnection()) {
               PreparedStatement statement = conn.prepareStatement(
                        "insert into person (Pin, LastName, FirstName, Gender, Token, AddressId) values (?, ?, ?, ?, ?, ?);",
                        Statement.RETURN_GENERATED_KEYS); 
               statement.setString(1, person.getPin());
               statement.setString(2, person.getLastName());
               statement.setString(3, person.getFirstName());
               statement.setString(4, person.getGender());
               statement.setString(5, person.getToken());
               statement.setLong(6, person.getAddress().getAddressId());
               statement.executeUpdate();
               ResultSet rs = statement.getGeneratedKeys();
               if (rs.next()) {
                   return rs.getLong(1);
               }
           } catch (SQLException e) {
               e.printStackTrace();
           }
        return (long) 0;
	}
	
	public static void update(Person person) {
        try (Connection conn = DBUtility.createConnection()) {
             PreparedStatement statement = conn.prepareStatement(
                     "update person set " +
                    		 "Pin = ?, " +
                             "LastName = ?, " +
                             "FirstName = ?, " +
                             "Gender = ?, " +
                             "Token = ? " +
                         "where personId = ?;",
                     Statement.NO_GENERATED_KEYS);
			statement.setString(1, person.getPin());
            statement.setString(2, person.getLastName());
            statement.setString(3, person.getFirstName());
            statement.setString(4, person.getGender());
            statement.setString(5, person.getToken());
            statement.setLong(6, person.getPersonId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

	
	public static void delete(Person person) {
        try (Connection conn = DBUtility.createConnection()) {
             PreparedStatement statement = conn.prepareStatement(
                     "delete FROM person " +
                             "where PersonId = ?;",
                     Statement.SUCCESS_NO_INFO);
            statement.setLong(1, person.getPersonId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}
