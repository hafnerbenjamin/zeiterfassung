package DAL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Project;
import model.Role;
import utility.DBUtility;

public class DBRole {
	public static ObservableList<Role> get(Long id) {
		String stat = "SELECT roleId, roleName, cost FROM Role ";
		if (id != null){
			stat += "where roleId = ?";
		}
        ObservableList<Role> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            if (id != null) { statement.setLong(1, id); }
            ResultSet set = statement.executeQuery();
            mapRole(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
	}


    private static void mapRole(ObservableList<Role> result, ResultSet set) throws SQLException
    {
        while (set.next())
        {
            Role role = new Role(
                    set.getLong(1),
                    set.getString(2),
                    set.getFloat(3)
            );
            result.add(role);
        }
    }
	
	public static Role getRoleForTimeEntryId(Long projectDetailId)
	{
		String stat = "SELECT roleId, roleName, cost FROM Role as r" + 
					  "join projectDetails as pd on pd.RoleId = r.RoleId " + 
					  "where pd.projectDetailId = ?";
        ObservableList<Role> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            statement.setLong(1, projectDetailId);
            ResultSet set = statement.executeQuery();
            mapRole(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
	}

    
	public static Long add(Role role) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                     "insert into Role (RoleName, Cost) values (?, ?);",
                     Statement.RETURN_GENERATED_KEYS); 
            statement.setString(1, role.getRoleName());
            statement.setFloat(2, role.getCost());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                return rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (long) 0;
	}

	public static void update(Role role) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "update role set " +
                   		 "RoleName = ?, " +
                         "Cost = ? " +
                        "where roleId = ?;",
                    Statement.NO_GENERATED_KEYS);
			statement.setString(1, role.getRoleName());
           statement.setFloat(2, role.getCost());
           statement.setLong(3, role.getRoleId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }	
	}

	public static void delete(Role role) {
        try (Connection conn = DBUtility.createConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "delete FROM role " +
                            "where roleId = ?;",
                    Statement.SUCCESS_NO_INFO);
           statement.setLong(1, role.getRoleId());
           statement.executeUpdate();
       } catch (SQLException e) {
           e.printStackTrace();
       }
	}
}
