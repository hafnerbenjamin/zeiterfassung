package DAL;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Address;
import model.Person;
import utility.DBUtility;

import java.sql.*;

// Obsolete
public class DBAddress {

	public static ObservableList<Address> get(Long id) {
		String stat = "SELECT addressId, street, streetnr, city, plz, country FROM Address ";
		if (id != null){
			stat += "where addressId = ?";
		}
        ObservableList<Address> result = FXCollections.observableArrayList();
        try (Connection conn = DBUtility.createConnection()){
            PreparedStatement statement = conn.prepareStatement(stat);
            if (id != null) { statement.setLong(1, id); }
            ResultSet set = statement.executeQuery();
            mapPerson(result, set);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
	}
	
    private static void mapPerson(ObservableList<Address> result, ResultSet set) throws SQLException
    {
        while (set.next())
        {
        	Address address = new Address(
                    set.getLong(1),
                    set.getString(2),
                    set.getString(3),
                    set.getString(4),
                    set.getString(5),
                    set.getString(6)
            );
            result.add(address);
        }
    }
	
	public static Long add(Address address) {
		try (Connection conn = DBUtility.createConnection()) {
			PreparedStatement statement = conn.prepareStatement(
					"insert into address (Street, StreetNr, Plz, City, Country) values (?, ?, ?, ?, ?);",
					Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, address.getStreet());
			statement.setString(2, address.getStreetNr());
			statement.setString(3, address.getPlz());
			statement.setString(4, address.getCity());
			statement.setString(5, address.getCountry());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			if (rs.next()) {
				return rs.getLong(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return (long) 0;
	}

	public static void update(Address address) {
		try (Connection conn = DBUtility.createConnection()) {
			PreparedStatement statement = conn.prepareStatement("update address set " + "Street = ?, "
					+ "StreetNr = ?, " + "Plz = ?, " + "City = ?, " + "Country = ? " + "where AddressId = ?;",
					Statement.NO_GENERATED_KEYS);
			statement.setString(1, address.getStreet());
			statement.setString(2, address.getStreetNr());
			statement.setString(3, address.getPlz());
			statement.setString(4, address.getCity());
			statement.setString(5, address.getCountry());
			statement.setLong(6, address.getAddressId());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void delete(Address address) {
		try (Connection conn = DBUtility.createConnection()) {
			PreparedStatement statement = conn.prepareStatement("delete from address " + "where AddressId = ?;",
					Statement.SUCCESS_NO_INFO);
			statement.setLong(1, address.getAddressId());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
