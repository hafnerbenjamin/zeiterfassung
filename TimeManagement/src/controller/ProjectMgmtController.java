package controller;

import java.util.Optional;

import BLL.BLLProject;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Person;
import model.Project;

public class ProjectMgmtController {

    @FXML
    private TableView<Project> tableView;
    @FXML
    private TableColumn<Project, String> nameColumn;
    @FXML
    private TableColumn<Project, String> nrColumn;
    @FXML
    private TableColumn<Project, String> activColumn;
    @FXML
    private TextField name; 
    @FXML
    private TextField nr; 
    @FXML
    private ChoiceBox<Object> activ;
    @FXML
    private Button deleteButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button newButton;
    
    Project actualLine = new Project();
    BLLProject bllOfClass = new BLLProject();

    public void initialize() {
        loadAllEntrys();
    	newEntry();

    	tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> choice(newValue));

    	
        saveButton.setOnAction(event -> {
        	save(actualLine);
        });

        deleteButton.setOnAction(event -> {   
        	delete(actualLine);
        });
        
        newButton.setOnAction(event -> {   
        	newEntry();
        });

    }
    
    private void newEntry() {
        name.setText("Name");
        nr.setText("Projektnr.");
        activ.setItems(FXCollections.observableArrayList("Status", new Separator(), "Aktiv", "Geschlossen"));
        activ.getSelectionModel().selectFirst();
	}

	private void delete(Project toDelete) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Project l�schen");
        alert.setHeaderText("Wollen sie das Project l�schen?");
        alert.setContentText(toDelete.getName());
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
        	bllOfClass.delete(toDelete);
            loadAllEntrys();
        }
	}

	private void save (Project toSave){
    	try{
    		if(bllOfClass.getById(toSave.getProjectId()) != null){
    			bllOfClass.update(toSave);
    		}else{
    			bllOfClass.add(toSave);
    		}
    		newEntry();
    		loadAllEntrys();
    	}catch (Exception e) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        alert.setTitle("Speicher nicht m�glich");
	        alert.setHeaderText("Es konnte nicht gespeichert werden.");
	        alert.setContentText("�berpr�fen sie die Vollst�ndigkeit und Datetypen der Felder");
	        alert.showAndWait();
    	        
    		System.out.println("cant be saved: " + toSave.getName());
		}
   	}


    private void choice(Project actualLine){
        try {
        	this.actualLine = actualLine;
            name.setText(actualLine.getName());
            nr.setText(Float.toString(actualLine.getProjectId()));
        	if(actualLine.getActive()){
	        	activ.getSelectionModel().select(2);
	        }else{
	        	activ.getSelectionModel().select(3);
	        }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void loadAllEntrys() {
        tableView.getItems().clear();
	    try{
	    	tableView.getItems().addAll(bllOfClass.get());        	
	    	createColumn();
	    }catch (Exception e) {
	    	System.out.println("the List form the DB is not found " + bllOfClass.toString());
		}
    }

    private void createColumn() {
    	nameColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getName()));
    	nrColumn.setCellValueFactory(model -> new SimpleStringProperty(Float.toString(model.getValue().getProjectId())));
    	activColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getActive())));
    	}
}
