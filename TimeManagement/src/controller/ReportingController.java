package controller;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

import BLL.BLLPerson;
import BLL.BLLProject;
import BLL.BLLServiceType;
import BLL.BLLTeam;
import BLL.BLLTimeEntry;
import application.Login;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Person;
import model.Project;
import model.ServiceType;
import model.Team;
import model.TimeEntry;

public class ReportingController {

    @FXML
    private TableView<TimeEntry> tableView;
    @FXML
    private TableColumn<TimeEntry, String> projectColumn;
    @FXML
    private TableColumn<TimeEntry, String> serviceTypeColumn;
    @FXML
    private TableColumn<TimeEntry, String> dateColumn;
    @FXML
    private TableColumn<TimeEntry, String> timeColumn;
    @FXML
    private TableColumn<TimeEntry, String> commentCloumn;
    @FXML
    private TableColumn<TimeEntry, String> activeColumn;
    @FXML
    private DatePicker fromDate; 
    @FXML
    private DatePicker toDate; 
    @FXML
    private ChoiceBox<Object> person;
    @FXML
    private Button printButton;
    @FXML
    private Button ok;

    ObservableList<Person> persons;
    TimeEntry actualLine = new TimeEntry();
    BLLTimeEntry bllOfClass = new BLLTimeEntry();
    
    BLLProject bllProjcet = new BLLProject();
    BLLPerson bllPerson = new BLLPerson();
    BLLServiceType bllServiceType = new BLLServiceType();

    public void initialize() {
        person.setItems(FXCollections.observableArrayList("alle Personen", new Separator()));
        try{
        	persons = bllPerson.get();
        	for(Person p : persons){
        		person.getItems().addAll(FXCollections.observableArrayList(p.getToken()));
	        }
        }catch (Exception e) {
		}
        person.getSelectionModel().selectFirst();
        
    	fromDate.setValue(LocalDate.of(2017, 01, 01));
    	toDate.setValue(LocalDate.of(2017, 12, 31));
    	loadEntrys(fromDate.getValue(), toDate.getValue());
    	
        printButton.setOnAction(event -> {   
        	print();
        });

        ok.setOnAction(event -> {   
        	loadEntrys(fromDate.getValue(), toDate.getValue());
        });

        
    }

    private void print() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("drucken");
        alert.setHeaderText("Ihr Report wird gedruckt");
        alert.setContentText(fromDate.getValue()+" - "+toDate.getValue());
        alert.showAndWait();
    	
    	System.out.println("Drucke report.........");		
	}


    public void loadEntrys(LocalDate from, LocalDate to) {
        tableView.getItems().clear();
        Person choisenPerson = null;
        try{
        	choisenPerson = persons.filtered(x -> x.getToken() == person.getSelectionModel().getSelectedItem()).get(0);
        }catch (Exception e) {
		}
        if(choisenPerson == null){
        	tableView.getItems().addAll(bllOfClass.getTimeLap(from, to, null));
        }else{
        	tableView.getItems().addAll(bllOfClass.getTimeLap(from, to, choisenPerson.getPersonId()));
        }
    	createColumn();
    }

    private void createColumn() {
    	projectColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getProject().getName()));
    	serviceTypeColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getServiceType().getName()));
    	dateColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getDate())));
    	timeColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getHours())));
    	commentCloumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getComment()));
    	activeColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getActive())));    }
}
