package controller;

import java.util.Optional;

import BLL.BLLPerson;
import BLL.BLLTeam;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import model.Person;
import model.Team;

public class PersonMgmtController {

    @FXML
    private TableView<Person> personTableView;
    @FXML
    private TableColumn<Person, String> genderColumn;
    @FXML
    private TableColumn<Person, String> fristnameColumn;
    @FXML
    private TableColumn<Person, String> nameColumn;
    @FXML
    private TableColumn<Person, String> tokenColumn;
    @FXML
    private TableColumn<Person, String> teamCloumn;
    @FXML
    private ChoiceBox<Object> gender;
    @FXML
    private TextField firstname;
    @FXML
    private TextField name; 
    @FXML
    private TextField token; 
    @FXML
    private TextField number;
    @FXML
    private ChoiceBox<Object> team; 
   
    @FXML
    private Button delete;
    @FXML
    private Button save;
    @FXML
    private Button neu;

    Person aktPerson = new Person();
    BLLPerson bllPerson = new BLLPerson();
    BLLTeam bllTeam = new BLLTeam();
	ObservableList<Team> teams = bllTeam.get();


    public void initialize() {
        loadAllPersonen();
    	newEntry();

        personTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> personAnwaelen(newValue));

        save.setOnAction(event -> {
        	savePerson(aktPerson);
        });

        delete.setOnAction(event -> {   
        	deletePerson(aktPerson);
        });
        
        neu.setOnAction(event -> {   
        	newEntry();
        });
    }
      
    private void newEntry() {
        gender.setItems(FXCollections.observableArrayList("Anrede", new Separator(), "Herr", "Frau"));
        gender.getSelectionModel().selectFirst();
        firstname.setText("Vorname");
        name.setText("Nachname");
        number.setText("Personal Nr.");
        token.setText("k�rzel");
        team.setItems(FXCollections.observableArrayList("Team", new Separator()));
        try{
        	for(Team t : teams){
	        	team.getItems().addAll(FXCollections.observableArrayList(t.getName()));
	        }
        }catch (Exception e) {
		}
        team.getSelectionModel().selectFirst();
	}

	private void deletePerson(Person personToDelete) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person l�schen");
        alert.setHeaderText("Wollen sie die Person l�schen?");
        alert.setContentText(personToDelete.getFirstName() + " "+ personToDelete.getLastName());
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
        	bllPerson.delete(personToDelete);
    		loadAllPersonen();
        }
	}

	private void savePerson (Person personToSave){
    	try{
    		
    		TextInputDialog password = new TextInputDialog("Passwort");
            password.setTitle("Passwort");
            password.setHeaderText("Bitte geben Sie ein Passwort an.");
            Optional<String> result = password.showAndWait();
            
            if (result.isPresent()){
                personToSave.setPin(result.get());
            }
            
    		if(bllPerson.getById(personToSave.getPersonId()) != null){
    			bllPerson.update(personToSave);
    		}else{
    			bllPerson.add(personToSave);
    		}
    		newEntry();
    		loadAllPersonen();
    	}catch (Exception e) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        alert.setTitle("Speicher nicht m�glich");
	        alert.setHeaderText("Es konnte nicht gespeichert werden.");
	        alert.setContentText("�berpr�fen sie die Vollst�ndigkeit und Datetypen der Felder");
	        alert.showAndWait();
    	        
    		System.out.println("cant be saved: " + personToSave.getLastName());
		}
   	}


    private void personAnwaelen(Person p){
        try {
            aktPerson = p;
            if(p.getGender().equals("m")){
                gender.getSelectionModel().select(2);
            }else{
                gender.getSelectionModel().select(3);
            }
            firstname.setText(p.getFirstName());
            name.setText(p.getLastName());
            token.setText(p.getToken());
            number.setText(String.valueOf(p.getPersonId()));
            
            for(Team t : teams){
            	for(Person pInTeam : t.getPersons()){
            		if(pInTeam.getPersonId() == p.getPersonId()){
            			team.getSelectionModel().select(t.getName());		
            		}
            	}
	        }
            
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void loadAllPersonen() {
        personTableView.getItems().clear();
        try{
        	personTableView.getItems().addAll(bllPerson.get());        	
        	createPersonColumn();
        }catch (Exception e) {
	    	System.out.println("the List form the DB is not found " + bllPerson.toString());
		}
    }

    private void createPersonColumn() {
    	genderColumn.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getGender()));
    	fristnameColumn.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getFirstName()));
    	nameColumn.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getLastName()));
    	tokenColumn.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getToken()));
    	teamCloumn.setCellValueFactory(person -> new SimpleStringProperty(bllTeam.getById(person.getValue().getTeamId()).getName()));
    }
}


