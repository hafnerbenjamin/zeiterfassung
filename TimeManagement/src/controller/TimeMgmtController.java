package controller;

import java.time.LocalDate;
import java.util.Optional;

import BLL.BLLProject;
import BLL.BLLServiceType;
import BLL.BLLTimeEntry;
import application.Login;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Project;
import model.ServiceType;
import model.TimeEntry;

public class TimeMgmtController {

    @FXML
    private TableView<TimeEntry> tableView;
    @FXML
    private TableColumn<TimeEntry, String> projectColumn;
    @FXML
    private TableColumn<TimeEntry, String> serviceTypeColumn;
    @FXML
    private TableColumn<TimeEntry, String> dateColumn;
    @FXML
    private TableColumn<TimeEntry, String> timeColumn;
    @FXML
    private TableColumn<TimeEntry, String> commentCloumn;
    @FXML
    private TableColumn<TimeEntry, String> activeColumn;
    @FXML
    private ChoiceBox<Object> project; 
    @FXML
    private ChoiceBox<Object> serviceType;
    @FXML
    private DatePicker date; 
    @FXML
    private TextField time; 
    @FXML
    private TextField comment;    
    @FXML
    private Button deleteButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button newButton;

    TimeEntry actualLine = new TimeEntry();
    BLLTimeEntry bllOfClass = new BLLTimeEntry();
    
    BLLProject bllProcjet = new BLLProject();
    BLLServiceType bllServiceType = new BLLServiceType();

    public void initialize() {
        loadAllEntrys();
    	newEntry();

    	tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> choice(newValue));

    	
        saveButton.setOnAction(event -> {
        	save(actualLine);
        });

        deleteButton.setOnAction(event -> {   
        	delete(actualLine);
        });
        
        newButton.setOnAction(event -> {   
        	newEntry();
        });

    }
    
    private void newEntry() {
        project.setItems(FXCollections.observableArrayList("Projekt", new Separator()));
        try{
        	ObservableList<Project> projects = bllProcjet.get();
        	for(Project p : projects){
	        	project.getItems().addAll(FXCollections.observableArrayList(p.getName()));
	        }
        }catch (Exception e) {
		}        
        project.getSelectionModel().selectFirst();
        
        serviceType.setItems(FXCollections.observableArrayList("Leistungsart", new Separator()));
        try{
        	ObservableList<ServiceType> sts = bllServiceType.get();
        	for(ServiceType st : sts){
	        	serviceType.getItems().addAll(FXCollections.observableArrayList(st.getName()));
	        }
        }catch (Exception e) {
		}
        serviceType.getSelectionModel().selectFirst();
        
        date.setValue(LocalDate.now());
        time.setText("Stunden");
        comment.setText("Kommentar");
	}

	private void delete(TimeEntry toDelete) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Zeiteintrag l�schen");
        alert.setHeaderText("Wollen sie den Zeiteintrag l�schen?");
        alert.setContentText(toDelete.getComment());
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
        	bllOfClass.delete(toDelete);
            loadAllEntrys();
        }
	}

	private void save (TimeEntry toSave){
    	try{
    		if(bllOfClass.getById(toSave.getTimeEntryId()) != null){
    			bllOfClass.update(toSave);
    		}else{
    			bllOfClass.add(toSave);
    		}
    		newEntry();
    		loadAllEntrys();
    	}catch (Exception e) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        alert.setTitle("Speicher nicht m�glich");
	        alert.setHeaderText("Es konnte nicht gespeichert werden.");
	        alert.setContentText("�berpr�fen sie die Vollst�ndigkeit und Datetypen der Felder");
	        alert.showAndWait();
    	        
    		System.out.println("cant be saved: " + toSave.getTimeEntryId());
		}
   	}


    private void choice(TimeEntry actualLine){
        try {
            this.actualLine = actualLine;
            
            project.getSelectionModel().select(actualLine.getProject().getName());
            serviceType.getSelectionModel().select(actualLine.getServiceType().getName());
            date.setValue(actualLine.getDate());
            time.setText(String.valueOf(actualLine.getHours()));
            comment.setText(actualLine.getComment());
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void loadAllEntrys() {
        tableView.getItems().clear();
    	tableView.getItems().addAll(bllOfClass.getTimeEntriesForPerson(Login.LOGIN_PERSON.getPersonId()));        	
    	createColumn();
    }

    private void createColumn() {
    	projectColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getProject().getName()));
    	serviceTypeColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getServiceType().getName()));
    	dateColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getDate())));
    	timeColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getHours())));
    	commentCloumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getComment()));
    	activeColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getActive())));
    }
}
