package controller;


import java.util.Optional;

import BLL.BLLPerson;

import BLL.BLLTeam;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Person;
import model.Team;

public class TeamMgmtController {

    @FXML
    private TableView<Team> tableView;
    @FXML
    private TableColumn<Team, String> nameColumn;
    @FXML
    private TableColumn<Team, String> tokenColumn;
    @FXML
    private TableColumn<Team, String> leaderColumn;
    @FXML
    private TextField name; 
    @FXML
    private TextField token; 
    @FXML
    private ChoiceBox<Object> leader;
    @FXML
    private Button deleteButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button newButton;
    
    Team actualLine = new Team();
    BLLTeam bllOfClass = new BLLTeam();
	BLLPerson bLLPerson = new BLLPerson();

    public void initialize() {
        loadAllEntrys();
    	newEntry();

    	tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> choice(newValue));

    	
        saveButton.setOnAction(event -> {
        	save(actualLine);
        });

        deleteButton.setOnAction(event -> {   
        	delete(actualLine);
        });
        
        newButton.setOnAction(event -> {   
        	newEntry();
        });

    }
    
    private void newEntry() {
        name.setText("Name");
        token.setText("K�rzel");
        leader.setItems(FXCollections.observableArrayList("Leiter", new Separator()));
        try{
			ObservableList<Person> persons = bLLPerson.get();
        	for(Person p : persons){
        		leader.getItems().addAll(FXCollections.observableArrayList(p.getToken()));
	        }
        }catch (Exception e) {
        	System.out.println("leaders not found");
		}
        leader.getSelectionModel().selectFirst();
	}

	private void delete(Team toDelete) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Team l�schen");
        alert.setHeaderText("Wollen sie das Team l�schen?");
        alert.setContentText(toDelete.getName());
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
        	bllOfClass.delete(toDelete);
        }
	}

	private void save (Team toSave){
    	try{
    		if(bllOfClass.getById(toSave.getTeamId()) != null){
    			bllOfClass.update(toSave);
    		}else{
    			bllOfClass.add(toSave);
    		}
    		newEntry();
    		loadAllEntrys();
    	}catch (Exception e) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        alert.setTitle("Speicher nicht m�glich");
	        alert.setHeaderText("Es konnte nicht gespeichert werden. �berpr�fen sie die Vollst�ndigkeit und Datetypen der Felder");
	        alert.showAndWait();
    	        
    		System.out.println("cant be saved: " + toSave.getName());
		}
   	}


    private void choice(Team actualLine){
        try {
        	this.actualLine = actualLine;
	            name.setText(actualLine.getName());
	            token.setText(actualLine.getToken());
	            leader.getSelectionModel().select(actualLine.getLeader().getToken());
        }catch (Exception e){
           System.out.println("actualLine not found");
        }
    }


    public void loadAllEntrys() {
        tableView.getItems().clear();
	    try{
	    	tableView.getItems().addAll(bllOfClass.get());        	
	    	createColumn();
	    }catch (Exception e) {
	    	System.out.println("the List form the DB is not found " + bllOfClass.toString());
		}
    }


    private void createColumn() {
    	nameColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getName()));
    	tokenColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getToken()));
    	leaderColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getLeader().getToken()));
    }
}


