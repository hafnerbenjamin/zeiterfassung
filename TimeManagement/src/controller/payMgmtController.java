package controller;

import java.util.Optional;

import BLL.BLLServiceType;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import model.Person;
import model.ServiceType;

public class payMgmtController {

    @FXML
    private TableView<ServiceType> tableView;
    @FXML
    private TableColumn<ServiceType, String> nameColumn;
    @FXML
    private TableColumn<ServiceType, String> moneyColumn;
    @FXML
    private TableColumn<ServiceType, String> activColumn;
    @FXML
    private TextField name; 
    @FXML
    private TextField money; 
    @FXML
    private ChoiceBox<Object> activ;
    @FXML
    private Button deleteButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button newButton;
    
    ServiceType actualLine = new ServiceType();
    BLLServiceType bllOfClass = new BLLServiceType();

    public void initialize() {
        loadAllEntrys();
    	newEntry();

    	tableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> choice(newValue));

    	
        saveButton.setOnAction(event -> {
        	save(actualLine);
        });

        deleteButton.setOnAction(event -> {   
        	delete(actualLine);
        });
        
        newButton.setOnAction(event -> {   
        	newEntry();
        });

    }
    
    private void newEntry() {
        name.setText("Name");
        money.setText("Preis pro Stunde");
        activ.setItems(FXCollections.observableArrayList("Stauts", new Separator(), "Aktiv", "Gesperrt"));
        activ.getSelectionModel().selectFirst();
	}

	private void delete(ServiceType toDelete) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("ServiceType l�schen");
        alert.setHeaderText("Wollen sie die kostenart l�schen?");
        alert.setContentText(toDelete.getName());
        Optional<ButtonType> result = alert.showAndWait();
        
        if (result.get() == ButtonType.OK){
        	bllOfClass.delete(toDelete);
            loadAllEntrys();
        }
	}

	private void save (ServiceType toSave){
    	try{
    		if(bllOfClass.getById(toSave.getServiceTypeId()) != null){
    			bllOfClass.update(toSave);
    		}else{
    			bllOfClass.add(toSave);
    		}
    		newEntry();
    		loadAllEntrys();
    	}catch (Exception e) {
    		Alert alert = new Alert(Alert.AlertType.ERROR);
	        alert.setTitle("Speicher nicht m�glich");
	        alert.setHeaderText("Es konnte nicht gespeichert werden.");
	        alert.setContentText("�berpr�fen sie die Vollst�ndigkeit und Datetypen der Felder");
	        alert.showAndWait();
    	        
    		System.out.println("cant be saved: " + toSave.getName());
		}
   	}


    private void choice(ServiceType actualLine){
        try {
        	this.actualLine = actualLine;
            if(actualLine.getActive()){
            	activ.getSelectionModel().select(2);
            }else{
            	activ.getSelectionModel().select(3);
            }
            name.setText(actualLine.getName());
            money.setText(Float.toString(actualLine.getCost()));

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void loadAllEntrys() {
        tableView.getItems().clear();
	    try{
	    	tableView.getItems().addAll(bllOfClass.get());        	
	    	createColumn();
	    }catch (Exception e) {
	    	System.out.println("the List form the DB is not found " + bllOfClass.toString());
		}
	}

    private void createColumn() {
    	nameColumn.setCellValueFactory(model -> new SimpleStringProperty(model.getValue().getName()));
    	moneyColumn.setCellValueFactory(model -> new SimpleStringProperty(Float.toString(model.getValue().getCost())));
    	activColumn.setCellValueFactory(model -> new SimpleStringProperty(String.valueOf(model.getValue().getActive())));
    }
}
