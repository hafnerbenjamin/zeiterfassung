package model;

import java.time.LocalDate;

import com.sun.swing.internal.plaf.basic.resources.basic;

import javafx.beans.property.SimpleIntegerProperty;

public class TimeEntry {
	private long TimeEntryId;
	private int Hours;
	private LocalDate Date;
	private Project Project;
	private Person Person;
	private String Comment;
	private boolean Active;
	private ServiceType ServiceType;
	private Role Role;
	
	public TimeEntry() {
		this.TimeEntryId = -1;
		this.Hours = 0;
		this.Date = LocalDate.now();
		this.Project = new Project();
		this.Person = new Person();
		this.Comment = "";
		this.Active = true;
		this.Role = new Role();
	}
	
	public TimeEntry(long timeEntryId, int hours, LocalDate date, Project project, model.Person person, String comment, boolean active, ServiceType serviceType) {
		this.TimeEntryId = timeEntryId;
		this.Hours = hours;
		this.Date = date;
		this.Project = project;
		this.Person = person;
		this.Comment = comment;
		this.Active = active;
		this.ServiceType = serviceType;
	}

	public long getTimeEntryId() {
		return TimeEntryId;
	}
	public void setTimeEntryId(long timeEntryId) {
		TimeEntryId = timeEntryId;
	}
	public int getHours() {
		return Hours;
	}
	public void setHours(int hours) {
		Hours = hours;
	}
	public LocalDate getDate() {
		return Date;
	}
	public void setDate(LocalDate date) {
		Date = date;
	}
	public Project getProject() {
		return Project;
	}
	public void setProject(Project project) {
		Project = project;
	}
	public Person getPerson() {
		return Person;
	}
	public void setPerson(Person person) {
		Person = person;
	}
	public String getComment() {
		return Comment;
	}
	public void setComment(String comment) {
		Comment = comment;
	}
	public boolean getActive() {
		return Active;
	}
	public void setActive(boolean active) {
		this.Active = active;
	}
	public ServiceType getServiceType() {
		return ServiceType;
	}
	public void setServiceType(ServiceType serviceType) {
		this.ServiceType = serviceType;
	}
	public Role getRole() {
		return Role;
	}
	public void setRole(Role role) {
		Role = role;
	}
}
