package model;

public class Project {
	private long ProjectId;
	private String Name;
	private ServiceType ServiceType;
	private Boolean Active;
	
	public Project()
	{
		this.ProjectId = -1;
		this.Name = "";
		this.ServiceType = new ServiceType();
		this.Active = false;
	}
	
	public Project(long projectId, String name, Boolean active) {
		this.ProjectId = projectId;
		this.Name = name;
		this.Active = active;
	}
	
	public long getProjectId() {
		return ProjectId;
	}
	public void setProjectId(long projectId) {
		ProjectId = projectId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public ServiceType getServiceType() {
		return ServiceType;
	}
	public void setServiceType(ServiceType serviceType) {
		ServiceType = serviceType;
	}
	public Boolean getActive() {
		return Active;
	}
	public void setActive(Boolean active) {
		Active = active;
	}
	
}
