package model;

public class Address {
	private long AddressId;
	private String Street;
	private String StreetNr;
	private String City;
	private String Plz;
	private String Country;
	
	public Address()
	{
		this.AddressId = -1;
		this.Street = "";
		this.StreetNr = "";
		this.City = "";
		this.Plz = "";
		this.Country = "";
	}
	
	public Address(long addressId, String street, String streetNr, String city, String plz, String country)
	{
		this.AddressId = addressId;
		this.Street = street;
		this.StreetNr = streetNr;
		this.City = city;
		this.Plz = plz;
		this.Country = country;
	}

	public long getAddressId() {
		return AddressId;
	}

	public void setAddressId(long addressId) {
		AddressId = addressId;
	}

	public String getStreet() {
		return Street;
	}

	public void setStreet(String street) {
		Street = street;
	}

	public String getStreetNr() {
		return StreetNr;
	}

	public void setStreetNr(String streetNr) {
		StreetNr = streetNr;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getPlz() {
		return Plz;
	}

	public void setPlz(String plz) {
		Plz = plz;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}
	
	
}
