package model;


import javafx.collections.ObservableList;
import utility.EmployeeType;

public class Team {
	private long TeamId;
	private String Name;
	private ObservableList<Person> Employees;
	private Person Leader;
	private String Token;
	
	public Team() {
		this.TeamId = -1;
		this.Name = "";
		this.Token = "";
	}
	
	public Team(long teamId, String name, String token, ObservableList<Person> teamMembers) {
		this.TeamId = teamId;
		this.Name = name;
		this.Token = token;
		this.Employees = teamMembers;
		for(Person p : teamMembers){
			if(p.getTeamFunction() == EmployeeType.TeamLeader){
				this.Leader = p;
			}
		}
		
	}
	
	public Team(long teamId, String name) {
		this.TeamId = teamId;
		this.Name = name;
	}
	
	public long getTeamId() {
		return TeamId;
	}
	public void setTeamId(long teamId) {
		TeamId = teamId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	public ObservableList<Person> getPersons() {
		return Employees;
	}
	public void setPersons(ObservableList<Person> employees) {
		Employees = employees;
	}
	
	public void setLeader(Person leader) {
		Leader = leader;
	}
	
	public Person getLeader(){
		return Leader;
	}
}
