package model;

import javafx.collections.ObservableList;
import utility.EmployeeType;

public class Person {
	private long PersonId;
	private String Pin;
	private String Gender;
	private String LastName;
	private String FirstName;
	private String Token;
	private Address Address;
	private long TeamId;
	private EmployeeType TeamFunction;
	
	public Person()
	{
		this.PersonId = -1;
		this.Pin = "";
		this.Gender = "";
		this.LastName = "";
		this.FirstName = "";
		this.Token = "";
		this.Address = new Address();
	}

	public Person(long personId, String pin, String gender, String name, String firstName, String token, Address address, long teamId) {
		this.PersonId = personId;
		this.Pin = pin;
		this.Gender = gender;
		this.LastName = name;
		this.FirstName = firstName;
		this.Token = token;
		this.Address = address;
		this.TeamId = teamId;
	}

	public long getPersonId() {
		return PersonId;
	}

	public void setPersonId(long personId) {
		PersonId = personId;
	}

	public String getPin() {
		return Pin;
	}

	public void setPin(String pin) {
		Pin = pin;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getToken() {
		return Token;
	}

	public void setToken(String token) {
		Token = token;
	}
	public Address getAddress() {
		return Address;
	}

	public void setAddress(Address address) {
		Address = address;
	}

	public long getTeamId() {
		return TeamId;
	}

	public void setTeam(long teamId) {
		TeamId = teamId;
	}
	public EmployeeType getTeamFunction() {
		return TeamFunction;
	}

	public void setTeamFunction(EmployeeType teamFunction) {
		TeamFunction = teamFunction;
	}
}
