	package model;

public class ServiceType {
	private long ServiceTypeId;
	private String Name;
	private float Cost;
	private boolean Active;
		
	public ServiceType()
	{
		this.ServiceTypeId = -1;
		this.Name = "";
		this.Cost = 0f;
	}
	
	public ServiceType(long serviceTypeId, String name, float cost, boolean active) {
		this.ServiceTypeId = serviceTypeId;
		this.Name = name;
		this.Cost = cost;
		this.Active = active;
	}
	
	public long getServiceTypeId() {
		return ServiceTypeId;
	}
	public void setServiceTypeId(long serviceTypeId) {
		ServiceTypeId = serviceTypeId;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public float getCost() {
		return Cost;
	}
	public void setCost(float cost) {
		Cost = cost;
	}
	
	public boolean getActive() {
		return Active;
	}
	public void setActive(boolean active) {
		this.Active = active;
	}
}
