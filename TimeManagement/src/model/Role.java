package model;

public class Role {
	private long RoleId;
	private String RoleName;
	private float Cost;
	
	public Role() {
		this.RoleId = -1;
		this.RoleName = "";
		this.Cost = 0f;
	}
	
	public Role(long roleId, String roleName, float cost) {
		this.RoleId = roleId;
		this.RoleName = roleName;
		this.Cost = cost;
	}

	public long getRoleId() {
		return RoleId;
	}
	public void setRoleId(long roleId) {
		RoleId = roleId;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public float getCost() {
		return Cost;
	}
	public void setCost(float cost) {
		Cost = cost;
	}
}
