package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtility {
	private static String ConnectionString = "jdbc:mysql://localhost:3306/time_management?autoReconnect=true&useSSL=false";
	private static String User = "root";
	private static String Password = "root";
	
	public static Connection createConnection()
	{
        Connection conn = null;
        try 
        {
            conn = DriverManager.getConnection(ConnectionString, User, Password);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return conn;
	}
}
