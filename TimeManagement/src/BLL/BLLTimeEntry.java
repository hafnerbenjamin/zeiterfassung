package BLL;

import java.time.LocalDate;

import DAL.DBTimeEntry;
import javafx.collections.ObservableList;
import model.TimeEntry;

public class BLLTimeEntry implements iDataAccessService<TimeEntry> {

	
	@Override
	public ObservableList<TimeEntry> get() {
		return DBTimeEntry.get(null);
	}

	public ObservableList<TimeEntry> getTimeEntriesForPerson(Long personId) {
		if(personId == -1){
			return DBTimeEntry.get(null);
		}else{
			return DBTimeEntry.get(null).filtered(x -> x.getPerson().getPersonId() == personId);
		}
	}
	
	public ObservableList<TimeEntry> getTimeLap(LocalDate from, LocalDate to, Long personId) {
		ObservableList<TimeEntry> entries = DBTimeEntry.get(null).filtered(x -> x.getDate().isAfter(from) && x.getDate().isBefore(to));
		if (personId != null){
			entries = entries.filtered(x -> x.getPerson().getPersonId() == personId);
		}
		return entries;
	}
	
	
	@Override
	public TimeEntry getById(Long id) {
		return DBTimeEntry.get(id).stream().findFirst().orElse(null);
	}
	
	@Override
	public void add(TimeEntry timeEntry) {
		Long id = DBTimeEntry.getProjectDetail(timeEntry);
		if (id == null)
			id = DBTimeEntry.addProjectDetail(timeEntry);
			
		DBTimeEntry.add(timeEntry, id);
	}

	@Override
	public void update(TimeEntry timeEntry) {
		Long id = DBTimeEntry.getProjectDetail(timeEntry);
		if (id == null)
			id = DBTimeEntry.addProjectDetail(timeEntry);
			
		DBTimeEntry.update(timeEntry, id);
	}

	@Override
	public void delete(TimeEntry timeEntry) {
		DBTimeEntry.delete(timeEntry);
	}
}
