package BLL;

import DAL.DBTeam;
import javafx.collections.ObservableList;
import model.Team;

public class BLLTeam implements iDataAccessService<Team> {

	@Override
	public ObservableList<Team> get() {
		return DBTeam.get(null);
	}

	@Override
	public Team getById(Long id) {
		return DBTeam.get(id).stream().findFirst().orElse(null);
	}
	
	@Override
	public void add(Team team) {
		DBTeam.add(team);
	}

	@Override
	public void update(Team team) {
		DBTeam.update(team);
	}

	@Override
	public void delete(Team team) {
		DBTeam.delete(team);
	}
}
