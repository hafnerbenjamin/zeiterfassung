package BLL;

import javafx.collections.ObservableList;

public interface iDataAccessService<T> {
    public ObservableList<T> get();

    public T getById(Long id);
    
    public void add(T object);

    public void update(T object);

    public void delete(T object);
}
