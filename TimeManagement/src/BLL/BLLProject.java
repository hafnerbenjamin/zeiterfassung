package BLL;

import DAL.DBProject;
import javafx.collections.ObservableList;
import model.Project;

public class BLLProject implements iDataAccessService<Project> {

	@Override
	public ObservableList<Project> get() {
		return DBProject.get(null);
	}

	@Override
	public Project getById(Long id) {
		return DBProject.get(id).stream().findFirst().orElse(null);
	}
	
	@Override
	public void add(Project project) {
		DBProject.add(project);	
	}

	@Override
	public void update(Project project) {
		DBProject.update(project);
	}

	@Override
	public void delete(Project project) {
		DBProject.delete(project);
	}
}
