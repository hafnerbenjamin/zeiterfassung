package BLL;

import DAL.DBServiceType;
import javafx.collections.ObservableList;
import model.ServiceType;

public class BLLServiceType implements iDataAccessService<ServiceType> {

	@Override
	public ObservableList<ServiceType> get() {
		return DBServiceType.get(null);
	}

	@Override
	public ServiceType getById(Long id) {
		return DBServiceType.get(id).stream().findFirst().orElse(null);
	}
	
	@Override
	public void add(ServiceType serviceType) {
		DBServiceType.add(serviceType);
	}

	@Override
	public void update(ServiceType serviceType) {
		DBServiceType.update(serviceType);	
	}

	@Override
	public void delete(ServiceType serviceType) {
		DBServiceType.delete(serviceType);
	}
}
