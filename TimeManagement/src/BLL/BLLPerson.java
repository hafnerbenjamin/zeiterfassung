package BLL;

import DAL.DBPerson;
import javafx.collections.ObservableList;
import model.Person;

public class BLLPerson implements iDataAccessService<Person> {

	@Override
	public ObservableList<Person> get() {
		return DBPerson.get(null);
	}
	
	public Person getById(Long id) {
		return DBPerson.get(id).stream().findFirst().orElse(null);
	}

	@Override
	public void add(Person person) {
		DBPerson.add(person);
	}

	@Override
	public void update(Person person) {
		DBPerson.update(person);
	}

	@Override
	public void delete(Person person) {
		DBPerson.delete(person);
	}
}
