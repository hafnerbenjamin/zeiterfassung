package BLL;

import javafx.collections.ObservableList;
import model.Address;
import DAL.DBAddress;

public class BLLAddress implements iDataAccessService<Address> {
	
	@Override
	public ObservableList<Address> get() {
		return DBAddress.get(null);
	}

	@Override
	public Address getById(Long id) {
		return DBAddress.get(id).stream().findFirst().orElse(null);
	}
	
	@Override
	public void add(Address address) {
		DBAddress.add(address);
	}

	@Override
	public void update(Address address) {
		DBAddress.update(address);
	}

	@Override
	public void delete(Address address) {
		DBAddress.delete(address);
	}
}
