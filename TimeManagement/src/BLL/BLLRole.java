package BLL;

import DAL.DBRole;
import model.Role;
import javafx.collections.ObservableList;

public class BLLRole implements iDataAccessService<Role> {

	@Override
	public ObservableList<Role> get() {
		return DBRole.get(null);
	}

	@Override
	public Role getById(Long id) {
		return DBRole.get(id).stream().findFirst().orElse(null);
	}
	
	@Override
	public void add(Role role) {
		DBRole.add(role);
	}

	@Override
	public void update(Role role) {
		DBRole.update(role);
	}

	@Override
	public void delete(Role role) {
		DBRole.delete(role);
	}
}
