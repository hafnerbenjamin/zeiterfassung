-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: time_management
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `timeentry`
--

DROP TABLE IF EXISTS `timeentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeentry` (
  `timeEntryId` int(6) NOT NULL AUTO_INCREMENT,
  `WorkedHours` int(6) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `ProjectId` int(6) DEFAULT NULL,
  `PersonId` int(6) DEFAULT NULL,
  `Comment` varchar(50) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `ServiceTypeId` int(6) DEFAULT NULL,
  PRIMARY KEY (`timeEntryId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeentry`
--

LOCK TABLES `timeentry` WRITE;
/*!40000 ALTER TABLE `timeentry` DISABLE KEYS */;
INSERT INTO `timeentry` VALUES (1,2,'2017-03-15',63,11,'test',1,22),(2,2,'2017-03-15',63,11,'this is a comment',1,22),(3,34,'2017-03-15',63,11,'this is a comment',1,22),(4,1,'2017-03-15',64,11,'this is a comment',1,23),(5,23,'2017-03-15',65,11,'this is a comment',1,23),(6,5,'2017-02-15',65,12,'this is a comment',1,23),(7,23,'2017-02-15',64,12,'this is a comment',1,23),(8,4,'2017-02-15',63,12,'this is a comment',1,24),(9,6,'2017-02-15',65,12,'this is a comment',1,24),(10,87,'2017-02-15',67,13,'this is a comment',1,24),(11,5,'2017-02-15',68,13,'this is a comment',1,25),(12,3,'2017-01-15',68,14,'this is a comment',1,25),(13,89,'2017-01-15',69,15,'this is a comment',1,26),(14,2,'2017-01-15',69,16,'this is a comment',1,26),(15,4,'2017-01-15',69,16,'this is a comment',1,26),(16,2,'2017-01-15',70,17,'this is a comment',1,27),(17,3,'2017-01-15',70,18,'this is a comment',1,27),(18,6,'2017-01-15',71,19,'this is a comment',1,27),(19,8,'2017-01-11',71,20,'this is a comment',1,26),(20,7,'2017-01-12',71,21,'this is a comment',1,25);
/*!40000 ALTER TABLE `timeentry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-18 18:11:41
