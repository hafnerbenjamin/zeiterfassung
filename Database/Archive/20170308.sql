CREATE DATABASE  IF NOT EXISTS `time_management` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `time_management`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: time_management
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `AddressId` int(11) NOT NULL AUTO_INCREMENT,
  `Street` varchar(45) DEFAULT NULL,
  `StreetNr` varchar(5) DEFAULT NULL,
  `City` varchar(45) DEFAULT NULL,
  `Plz` varchar(10) DEFAULT NULL,
  `Country` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`AddressId`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (61,'Hauptstrasse','10','Aarau','1234','Schweiz'),(62,'Hauptstrasse','10','Aarau','1234','Schweiz'),(63,'Hauptstrasse','10','Aarau','1234','Schweiz'),(64,'Hauptstrasse','10','Aarau','1234','Schweiz'),(65,'Hauptstrasse','10','Aarau','1234','Schweiz');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `PersonId` int(11) NOT NULL AUTO_INCREMENT,
  `Pin` varchar(10) DEFAULT NULL,
  `Gender` varchar(10) DEFAULT NULL,
  `LastName` varchar(45) DEFAULT NULL,
  `FirstName` varchar(45) DEFAULT NULL,
  `AddressId` int(11) DEFAULT NULL,
  `Token` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`PersonId`),
  KEY `fk_person_address_idx` (`AddressId`),
  CONSTRAINT `fk_person_address` FOREIGN KEY (`AddressId`) REFERENCES `address` (`AddressId`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (11,'1111','m','hans','Nötig',61,'hno'),(12,'1111','m','hans','Nötig',61,'hno'),(13,'1111','m','hans','Nötig',61,'hno'),(14,'1111','m','hans','Nötig',61,'hno'),(15,'1111','m','hans','Nötig',61,'hno'),(16,'1111','m','hans','Nötig',61,'hno'),(17,'1111','m','hans','Nötig',61,'hno'),(18,'1111','m','hans','Nötig',61,'hno'),(19,'1111','m','hans','Nötig',61,'hno'),(20,'1111','m','hans','Nötig',61,'hno');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `personaddressview`
--

DROP TABLE IF EXISTS `personaddressview`;
/*!50001 DROP VIEW IF EXISTS `personaddressview`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `personaddressview` AS SELECT 
 1 AS `PersonId`,
 1 AS `AddressId`,
 1 AS `Pin`,
 1 AS `Gender`,
 1 AS `LastName`,
 1 AS `FirstName`,
 1 AS `Token`,
 1 AS `Street`,
 1 AS `StreetNr`,
 1 AS `City`,
 1 AS `Plz`,
 1 AS `value`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `personteam`
--

DROP TABLE IF EXISTS `personteam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personteam` (
  `PersonId` int(11) NOT NULL,
  `Team` int(11) NOT NULL,
  `Type` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PersonId`,`Team`),
  KEY `fk_PersonTeam_Team_idx` (`Team`),
  CONSTRAINT `fk_PersonTeam_Person` FOREIGN KEY (`PersonId`) REFERENCES `person` (`PersonId`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_PersonTeam_Team` FOREIGN KEY (`Team`) REFERENCES `team` (`TeamId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personteam`
--

LOCK TABLES `personteam` WRITE;
/*!40000 ALTER TABLE `personteam` DISABLE KEYS */;
/*!40000 ALTER TABLE `personteam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `ProjectId` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectName` varchar(45) DEFAULT NULL,
  `ServiceTypeId` int(11) DEFAULT NULL,
  `Active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`ProjectId`),
  KEY `fk_Project_ServiceType_idx` (`ServiceTypeId`),
  CONSTRAINT `fk_Project_ServiceType` FOREIGN KEY (`ServiceTypeId`) REFERENCES `servicetype` (`ServiceTypeId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (63,'Project_0',22,''),(64,'Project_1',22,''),(65,'Project_2',22,''),(66,'Project_3',22,''),(67,'Project_4',22,''),(68,'Project_5',22,''),(69,'Project_6',22,''),(70,'Project_7',22,''),(71,'Project_8',22,''),(72,'Project_9',22,''),(73,'Project_0',23,''),(74,'Project_1',23,''),(75,'Project_2',23,''),(76,'Project_3',23,''),(77,'Project_4',23,''),(78,'Project_5',23,''),(79,'Project_6',23,''),(80,'Project_7',23,''),(81,'Project_8',23,''),(82,'Project_9',23,'');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projectdetail`
--

DROP TABLE IF EXISTS `projectdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectdetail` (
  `RoleId` int(11) NOT NULL,
  `PersonId` int(11) NOT NULL,
  `ProjectDetailId` int(11) NOT NULL AUTO_INCREMENT,
  `ProjectId` int(11) NOT NULL,
  PRIMARY KEY (`ProjectDetailId`),
  KEY `fk_Person_Role_Role_idx` (`RoleId`),
  KEY `fk_Person_Role_Person_idx` (`PersonId`),
  KEY `fk_ProjectDetails_Project_idx` (`ProjectId`),
  CONSTRAINT `fk_ProjectDetail_Person` FOREIGN KEY (`PersonId`) REFERENCES `person` (`PersonId`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_ProjectDetail_Project` FOREIGN KEY (`ProjectId`) REFERENCES `project` (`ProjectId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ProjectDetail_Role` FOREIGN KEY (`RoleId`) REFERENCES `role` (`RoleId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projectdetail`
--

LOCK TABLES `projectdetail` WRITE;
/*!40000 ALTER TABLE `projectdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `projectdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(45) DEFAULT NULL,
  `Cost` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Role0',50),(2,'Role1',50);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicetype`
--

DROP TABLE IF EXISTS `servicetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicetype` (
  `ServiceTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `Cost` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`ServiceTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicetype`
--

LOCK TABLES `servicetype` WRITE;
/*!40000 ALTER TABLE `servicetype` DISABLE KEYS */;
INSERT INTO `servicetype` VALUES (22,'Pause',0),(23,'Urlaub',0),(24,'Admin',0);
/*!40000 ALTER TABLE `servicetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `TeamId` int(11) NOT NULL AUTO_INCREMENT,
  `TeamName` varchar(45) DEFAULT NULL,
  `AddressId` int(11) DEFAULT NULL,
  `Active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`TeamId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (1,'Team0',NULL,''),(2,'Team1',NULL,'');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeentry`
--

DROP TABLE IF EXISTS `timeentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeentry` (
  `TimeEntryId` int(11) NOT NULL AUTO_INCREMENT,
  `WorkedHours` int(11) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `ProjectDetailId` int(11) DEFAULT NULL,
  `Comment` varchar(2000) DEFAULT NULL,
  `Active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`TimeEntryId`),
  KEY `fk_TimeEntry_ProjectDetails_idx` (`ProjectDetailId`),
  CONSTRAINT `fk_TimeEntry_ProjectDetails` FOREIGN KEY (`ProjectDetailId`) REFERENCES `projectdetail` (`ProjectDetailId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeentry`
--

LOCK TABLES `timeentry` WRITE;
/*!40000 ALTER TABLE `timeentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'time_management'
--

--
-- Dumping routines for database 'time_management'
--

--
-- Final view structure for view `personaddressview`
--

/*!50001 DROP VIEW IF EXISTS `personaddressview`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `personaddressview` AS select `p`.`PersonId` AS `PersonId`,`a`.`AddressId` AS `AddressId`,`p`.`Pin` AS `Pin`,`p`.`Gender` AS `Gender`,`p`.`LastName` AS `LastName`,`p`.`FirstName` AS `FirstName`,`p`.`Token` AS `Token`,`a`.`Street` AS `Street`,`a`.`StreetNr` AS `StreetNr`,`a`.`City` AS `City`,`a`.`Plz` AS `Plz`,`a`.`Country` AS `value` from (`person` `p` left join `address` `a` on((`p`.`AddressId` = `a`.`AddressId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-08 19:04:16
