CREATE DATABASE  IF NOT EXISTS `time_management` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `time_management`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: time_management
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `personaddressview`
--

DROP TABLE IF EXISTS `personaddressview`;
/*!50001 DROP VIEW IF EXISTS `personaddressview`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `personaddressview` AS SELECT 
 1 AS `PersonId`,
 1 AS `AddressId`,
 1 AS `Pin`,
 1 AS `Gender`,
 1 AS `LastName`,
 1 AS `FirstName`,
 1 AS `Token`,
 1 AS `Street`,
 1 AS `StreetNr`,
 1 AS `City`,
 1 AS `Plz`,
 1 AS `value`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `personaddressview`
--

/*!50001 DROP VIEW IF EXISTS `personaddressview`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `personaddressview` AS select `p`.`PersonId` AS `PersonId`,`a`.`AddressId` AS `AddressId`,`p`.`Pin` AS `Pin`,`p`.`Gender` AS `Gender`,`p`.`LastName` AS `LastName`,`p`.`FirstName` AS `FirstName`,`p`.`Token` AS `Token`,`a`.`Street` AS `Street`,`a`.`StreetNr` AS `StreetNr`,`a`.`City` AS `City`,`a`.`Plz` AS `Plz`,`a`.`Country` AS `value` from (`person` `p` left join `address` `a` on((`p`.`AddressId` = `a`.`AddressId`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'time_management'
--

--
-- Dumping routines for database 'time_management'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-08 19:02:07
